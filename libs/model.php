<?php
class model{
    protected $connect;
    protected $database;
    protected $table;
    protected $resultQuery;

    public function __contruct($params = null){
        if($params == null){
            $params['server'] 		= DB_HOST;
            $params['username'] 	= DB_USER;
            $params['password']		= DB_PASS;
            $params['database'] 	= DB_NAME;
            $params['table']		= DB_TABLE;
        }
        $link = mysql_connect($params['server'], $params['username'], $params['password']);
        if(!$link){
            die('CouldCould not connect: '. mysql_error );
        }else{
            $this->connect = $link;
            $this->database = $params['database'];
            $this->table 	= $params['table'];
            $this->setDatabase();
        }
    }
    /*********************************
     * SET CONNECT
     */
    public function setConnect($connect){
        $this->connect = $connect;
    }

    /*********************************
     * SET DATABASE
     */
    public function setDatabase($database = null){
        if($database != null){
            $this->database = $database;
        }
        mysql_select_db($this->database, $this->connect);
    }

    /*********************************
     * SET TABLE
     */
    public function setTable($table){
        $this->table = $table;
    }

    /*********************************
     * QUERY
     */
    public function query($query){
        $this->resultQuery = mysql_query($query, $this->connect);
        return $this->resultQuery;
    }

    /*********************************
     * IS EXIST
     */
    public function isExist($query){
        if($query != null){
            $this->resultQuery = mysql_query($query, $this->connect);
        }
        if(mysql_num_rows($this->resultQuery) > 0){
            return true;
        }
        return false;
    }

    /*********************************
     * DESTRUCT CONNECT
     */
    public function __destruct(){
        mysql_close($this->connect);
    }

    // LIST RECORE
    public function listRecord($query){
        $result = array();
        if(!empty($query)){
            $resultQuery = $this->query($query);
            if(mysql_num_rows($resultQuery) > 0){
                while($row = mysql_fetch_assoc($resultQuery)){
                    $result[] = $row;
                }
                mysql_free_result($resultQuery);
            }
        }
        return $result;
    }

    // DELETE
    public function delete($where){

        $newWhere 	= $this->createWhereDeleteSQL($where);
        $query 		= "DELETE FROM `$this->table` WHERE `id` IN ($newWhere)";
        $this->query($query);
        return $this->affectedRows();
    }

    // CREATE WHERE DELTE SQL
    public function createWhereDeleteSQL($data){
        $newWhere = '';
        if(!empty($data)){
            foreach($data as $id) {
                $newWhere .= "'" . $id . "', ";
            }
            $newWhere .= "'0'";
        }
        return $newWhere;
    }

}

?>