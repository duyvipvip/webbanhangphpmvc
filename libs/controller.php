<?php
    class controller{
        protected     $_model;
        protected     $_view;

        //GET VIEW
        public function getView()
        {
            return $this->_view;
        }
        protected     $_arrPrams;
        protected     $_Template;

        public function __construct($arrPrams)
        {

            $this->setParams($arrPrams);
            $this->creatModel($this->_arrPrams['module'], $this->_arrPrams['controller']);

            $this->creatView( $this->_arrPrams['module']);
            $this->_view->_arrParams = $arrPrams;

            $this->creatTemplate($this);
        }

        //SET MODEL OBJECT
        public function creatModel($moduleName , $modelName){
            $modelName = $modelName . 'Model';
            $file = MODULE_PATH . $moduleName . DS . 'models' . DS . $modelName .'.php';
            if(file_exists($file)){
                require_once $file;

                $this->_model = new $modelName();
            }
        }

        //CREART VIEW OBJECT
        public function creatView($moduleName){
            $this->_view = new view($moduleName);
        }

        //SET PARAMS
        public function setParams($arr){
            $this->_arrPrams = $arr;
        }

        public function creatTemplate($controller){
            $this->_Template = new template($controller);
        }
    }
?>