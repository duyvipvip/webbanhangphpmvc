<?php
    class template{
        private $_controllerObject;
        private $_fileTemplate;



        private $_folderTemplate;
        private $_configTemplate;

        public function __construct($controller)
        {
            $this->_controllerObject = $controller;

        }

        public function load(){
            $fileConfig         = $this->getConfigTemplate();
            $fileTemplate       = $this->getFileTemplate();
            $folderTemplate     = $this->getFolderTemplate();

            $pathFileConfig = TEMPLATE_PATH . $folderTemplate . $fileConfig;
            if(file_exists($pathFileConfig)){
                $arrConfig = parse_ini_file($pathFileConfig);
                $view = $this->_controllerObject->getView();
                $view->_fileCss = $this->css($arrConfig['dirCss'], $arrConfig['fileCss'], "css");
                $view->_fileJs = $this->css($arrConfig['dirJs'], $arrConfig['fileJs'], "js");
                $view->_metaHTTP = $view->createMeta($arrConfig["metaHTTP"], "http-equiv");
                $view->_metaName = $view->createMeta($arrConfig["metaName"], "name");
                $view->_title    = $arrConfig["title"];
                $view->_folderImage = TEMPLATE_URL . $this->_folderTemplate . "images".DS;
            }



            $view->_templatePath = TEMPLATE_PATH . $folderTemplate . $fileTemplate;

        }
        public function css($dirFolder, $array, $type = "css"){
            $xhtml = "";
            $path = TEMPLATE_URL . $this->_folderTemplate . $dirFolder. DS;
            if(!empty($array)){
                foreach ($array as $item){
                    if($type=="css"){
                        $xhtml .= '<link href="'.$path.$item.'" rel="stylesheet">';
                    }else if($type ==  "js"){

                        $xhtml .=  '<script src="'.$path.$item.'"></script>';
                    }

                }
                return $xhtml;
            }

        }
        // SET FILE TEMPLATE "default: index.php"
        public function setFileTemplate($fileTemplate = "index.php")
        {
            $this->_fileTemplate = $fileTemplate;
        }

        // SET FOLDER TEMPLATE "default: client/main"
        public function setFolderTemplate($folderTemplate ="client/main/")
        {
            $this->_folderTemplate = $folderTemplate;
        }

        // SET FILE CONFIG TEMPLATE "default: template.ini"
        public function setConfigTemplate($configTemplate = "template.ini")
        {
            $this->_configTemplate = $configTemplate;
        }

        public function getFileTemplate()
        {
            return $this->_fileTemplate;
        }

        public function getFolderTemplate()
        {
            return $this->_folderTemplate;
        }

        public function getConfigTemplate()
        {
            return $this->_configTemplate;
        }
    }
?>