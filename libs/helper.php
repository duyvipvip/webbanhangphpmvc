<?php
    class helper{

        // CREATE BUTTON IN HEADER CONTENT
        public static function cmsButton($name , $id , $link , $icon , $type = "new"){
            $xhtml ='<li class="button" id='.$id.'>';
            if($type == "new"){
                $xhtml .= '<a  href='.$link.'><span class='.$icon.'></span>'.$name.'</a>';
            }else if($type = 'submit'){
                $xhtml .= '<a onclick="javascript:submitForm(\''.$link.'\');"><span class='.$icon.'></span>'.$name.'</a>';
            }

            $xhtml .= '</li>';

            return $xhtml;
        }

        // FORMAT DATE
        public static function formatDate($format,$value){
            $result = '';
            if(!empty($value) && $value != "0000-00-00"){
                $result = date_format(date_create($value), "d-m-Y" );

            }
            return $result;
        }

        // CREATE STRING HTML GROUP STATUS
        public static function cmsGroupStatus($valueStatus, $urlHandl , $id){
            $iconStatus = $valueStatus == 0 ? "unpublish" : "publish";
            $status     = '<a class ="jgrid" id="groupstatus-'.$id.'" href="javascript:changeGroupStatus(\''.$urlHandl.'\');">
                            <span class="state '.$iconStatus.'"></span>
                        </a>';

            return $status;
        }

        // CREATE STRING HTML GROUP ACP
        public static function cmsGroupAcp($valueStatus, $urlHandl , $id){
            $iconStatus = $valueStatus == 0 ? "unpublish" : "publish";
            $status     = '<a class ="jgrid" id="groupAcp-'.$id.'" href="javascript:changeGroupAcp(\''.$urlHandl.'\');">
                            <span class="state '.$iconStatus.'"></span>
                        </a>';

            return $status;
        }
    }
?>