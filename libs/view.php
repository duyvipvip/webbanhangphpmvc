<?php
    class view{
        public  $_templatePath;
        public  $_fileCss;
        public  $_fileJs;
        public  $_folderImage;
        public  $_metaHTTP;
        public  $_metaName;
        public  $_title;
        public  $_contentView;
        public  $_moduleName;
        public  $_arrParams;


        public function __construct($moduleName)
        {
            $this->_moduleName  = $moduleName;
        }
        //RENDER
        public function render($fileInclude, $full= true){

            // url file view in folder views
            $path = MODULE_PATH . $this->_moduleName . DS . 'views' . DS . $fileInclude . '.php';
            if(file_exists($path)){
                if($full == true){
                    $this->_templatePath;
                    if(file_exists($this->_templatePath)){

                        $this->_contentView = $path;
                        //require file view in folder template
                        require_once $this->_templatePath;
                    };
                }
                else{
                    require_once $path;
                }

            }else{
                echo __METHOD__. ': error';
            }

        }

        //CREATE META example:meta name,content
        public function createMeta($arrMeta, $name){
            $xhtml ='';
            if(!empty($arrMeta)){
                foreach ($arrMeta as $meta){
                    $temp = explode('|', $meta);
                    $xhtml .= '<meta '.$name.'="'.$temp[0].'" content="'.$temp[1].'" />';
                }
            }
            return $xhtml;
        }

        //APPPEND CSS
        public function appendCss($array){
            $path = MODULE_URL . $this->_moduleName . DS . "views" . DS;
            $xhtml ="";
            if(!empty($array)){
                foreach($array as $item){
                    $xhtml .= '<link rel="stylesheet" type="text/css" href="'.$path.$item.'"/>';
                }
            }

            empty($this->_fileCss) ? $this->_css = $xhtml : $this->_fileCss .= $xhtml;

        }
    }
?>