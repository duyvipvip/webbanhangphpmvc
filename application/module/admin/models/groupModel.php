<?php

class groupModel extends model {
    protected $_tableName = 'group';
    public function __construct(){
        parent::__contruct();
        $this->setTable($this->_tableName);
    }

    // Show list item group to index
    public function listItems(){
        $query[]    = "SELECT *";
        $query[]    = "FROM `$this->table`";
        echo $query      = implode(' ',$query);
        $result = $this->listRecord($query);
        return $result;
    }

    // Change status group
    public function changeStatus($arrParams, $option = null){
        if($option["task"]  == "change-status"){
            $status     = ($arrParams["status"] == 0) ? 1 : 0;
            $id         = ($arrParams["id"]);
            $query      = 'UPDATE `group` SET `status` = "'.$status.'" WHERE `id` = "'.$id.'"';
            $this->query($query);

            $url =  $urlHandl   =   url::createLink("admin", "group", "ajaxGroupStatus", array('id'=>$id, 'status'=>$status));
            return array($id,$status, $url);
        }
        if($option["task"]  == "change-status-groupAcp"){
            $status     = ($arrParams["groupAcp"] == 0) ? 1 : 0;
            $id         = ($arrParams["id"]);
            $query      = 'UPDATE `group` SET `group_acp` = "'.$status.'" WHERE `id` = "'.$id.'"';
            $this->query($query);

            $url =  $urlHandl   =   url::createLink("admin", "group", "ajaxGroupAcp", array('id'=>$id, 'groupAcp'=>$status));
            return array($id , $status , $url);
        }

        if($option["task"]  == "change-mulity-status"){
            $status     = $arrParams["type"] == 0 ? 0 : 1;
            if(!empty($arrParams['cid'])){
                $ids = $this->createWhereDeleteSQL($arrParams['cid']);
                $query = 'UPDATE `group` SET `status` = "'.$status.'" WHERE `id` IN ('.$ids.')';
                $this->query($query);
            }
        }

    }

    // Delete item group
    public function deleteItem($arrParams, $option = null){
        if($option  == null){
            if(!empty($arrParams['cid'])){
                $ids = $this->createWhereDeleteSQL($arrParams['cid']);
                $query = 'DELETE FROM `group`WHERE `id` IN ('.$ids.')';
                $this->query($query);
            }
        }

    }
}