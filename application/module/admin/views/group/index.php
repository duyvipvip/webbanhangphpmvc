
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header -->
    <?php include_once "contentHeader/index.php"; ?>
    <!-- Content Header -->

    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" id="box-form">
                        <div class="list">
                            <form action="#" method="post" id="mainForm">
                                <div class="row head" >
                                    <p class="w-10"><input type="checkbox" name="check-all-toggle" id="check-all"></p>
                                    <p>Group Name</p>
                                    <p class="w-10">Status</p>
                                    <p class="w-10">Group ACP</p>
                                    <p class="w-10">Ordering</p>
                                    <p class="w-10">Created</p>
                                    <p class="w-10">Created By</p>
                                    <p class="w-10">Modified</p>
                                    <p class="w-10">Modified By</p>
                                </div>
                                <?php
                                if(!empty($this->listGroup)) {
                                    $i = 0;
                                    foreach ($this->listGroup as $key => $value) {
                                        $id = $value['id'];
                                        $ckb = '<input type="checkbox" name="checkbox[]" value="' . $id . '">';
                                        $name = $value['name'];
                                        $row = ($i % 2) == 0 ? "row0" : "row1";

                                        // urlStatus index.php?module=admin&controller=group&action=ajaxStatus&id=2&status=0
                                        $urlStatus = url::createLink("admin", "group", "ajaxGroupStatus", array('id' => $id, 'status' => $value["status"]));
                                        $status = helper::cmsGroupStatus($value["status"], $urlStatus, $id);

                                        $urlAcp = url::createLink("admin", "group", "ajaxGroupAcp", array('id' => $id, 'groupAcp' => $value["group_acp"]));
                                        $groupAcp = helper::cmsGroupAcp($value['group_acp'], $urlAcp, $id);

                                        $ordering = '<input type="text" name="order[]" size="5" value="' . $value['ordering'] . '">';
                                        $createdBy = $value["created_by"];
                                        $modifiedBy = $value["modified_by"];

                                        $created = helper::formatDate("d-m-Y", $value['created']);
                                        $modified = helper::formatDate("d-m-Y", $value['modified']);


                                        echo '<div class="row ' . $row . '" id="item-' . $i . '">
                                            <p class="w-10"><input type="checkbox" name="cid[]" value="'.$id.'"></p>
                                            <p>' . $name . '</p>
                                            <p class="w-10">' . $status . '</p>
                                            <p class="w-10">' . $groupAcp . '</p>
                                            <p class="w-10">' . $ordering . '</p>
                                            <p class="w-10">' . $created . '</p>
                                            <p class="w-10">' . $createdBy . '</p>
                                            <p class="w-10">' . $modified . '</p>
                                            <p class="w-10">' . $modifiedBy . '</p>
                                        </div>';
                                        $i++;
                                    }
                                }
                                ?>

                            </form>

                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
</div>