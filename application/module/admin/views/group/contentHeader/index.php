<?php
    $linkNew = url::createLink("admin", "group", "add");
    $btnNew = helper::cmsButton("New", "toolbar-popup-new", $linkNew, "icon-32-new");

    //PUBLIC
    $linkPublic = url::createLink("admin", "group", "mulityStatus", array('type'=>1));
    $btnPublic = helper::cmsButton("Publish", "toolbar-publish", $linkPublic, "icon-32-publish", 'submit');

    //UNPUBLIC
    $linkUnPublic = url::createLink("admin", "group", "mulityStatus" ,  array('type'=>0));
    $btnUnPublic = helper::cmsButton("Unpublish", "toolbar-unpublish", $linkUnPublic, "icon-32-unpublish", 'submit');

    //Ordering
    $linkOrdering = url::createLink("admin", "group", "index");
    $btnOrdering = helper::cmsButton("Ordering", "toolbar-checkin", $linkOrdering, "icon-32-checkin");

    //Trash
    $linkTrash = url::createLink("admin", "group", "trash");
    $btnTrash = helper::cmsButton("Trash", "toolbar-trash", $linkTrash, "icon-32-trash", 'submit');

    //Save
    $linkSave = url::createLink("admin", "group", "index");
    $btnSave = helper::cmsButton("Save", "toolbar-apply", $linkSave, "icon-32-apply");

    //Save And Close
    $linkSaveClose = url::createLink("admin", "group", "index");
    $btnSaveClose = helper::cmsButton("Save And Close", "toolbar-save", $linkSaveClose, "icon-32-save");

    //Save And New
    $linkSaveClose = url::createLink("admin", "group", "index");
    $btnSaveNew = helper::cmsButton("Save And New", "toolbar-save-new", $linkSaveClose, "icon-32-save-new");

    //Cancel
    $linkCancel = url::createLink("admin", "group", "index");
    $btnCancel = helper::cmsButton("Cancel", "toolbar-cancel", $linkCancel, "icon-32-cancel");

    switch ($this->_arrParams["action"]) {
        case "index":
            $strButton = $btnNew . $btnPublic . $btnUnPublic . $btnOrdering . $btnTrash;
            break;
        case "add":
            $strButton = $btnSave . $btnSaveClose . $btnSaveNew . $btnCancel;
            break;
    }
?>

<section class="content-header">
    <h1>
        <?php echo $this->_title; ?>
        <small>advanced tables</small>
    </h1>

    <div class="toolbar-list" id="toolbar">
        <ul>
            <?php echo $strButton; ?>
        </ul>

        <div class="clr"></div>
    </div>
</section>