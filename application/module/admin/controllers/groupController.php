<?php
class groupController extends controller {
    public function __construct($arrPrams)
    {
        parent::__construct($arrPrams);
        $this->_Template->setConfigTemplate("template.ini");
        $this->_Template->setFolderTemplate("admin/main/");
        $this->_Template->setFileTemplate("group.php");
        $this->_Template->load();
    }

    // Function main indexAction
    public function indexAction(){

        $this->_view->_title = "Group User";
        $this->_view->appendCss(array("group/css/style.css"));
        $this->_view->listGroup = $this->_model->listItems();
        $this->_view->render('group/index', true);


    }

    // Function addAction use add group
    public function addAction(){
        $this->_view->_title = "Group User Add";
        $this->_view->appendCss(array("group/css/style.css"));
        $this->_view->render('group/add', true);
    }

    // Function ajax Group Status Action
    public function ajaxGroupStatusAction(){
        $result     = $this->_model->changeStatus($this->_arrPrams, array("task"=>"change-status"));
        echo json_encode($result);
    }

    // Function ajax Group Status
    public function ajaxGroupAcpAction(){
        $result     = $this->_model->changeStatus($this->_arrPrams, array("task"=>"change-status-groupAcp"));
        echo json_encode($result);
    }

    // Change mulity statusTrash group
    public function mulityStatusAction(){
        $result     = $this->_model->changeStatus($this->_arrPrams, array("task"=>"change-mulity-status"));
        url::redirect(url::createLink("admin", "group", 'index'));
    }

    // Trash group
    public function trashAction(){
        $this->_model->deleteItem($this->_arrPrams);
        url::redirect(url::createLink("admin", "group", 'index'));
    }
}
?>