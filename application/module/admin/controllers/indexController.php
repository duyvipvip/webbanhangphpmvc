<?php
class indexController extends controller {
    public function __construct($arrPrams)
    {
        parent::__construct($arrPrams);

    }

    public function indexAction(){
        $this->_Template->setConfigTemplate("template.ini");
        $this->_Template->setFolderTemplate("admin/main/");
        $this->_Template->setFileTemplate("index.php");
        $this->_Template->load();
        $this->_view->render('index/index', true);
    }
}
?>