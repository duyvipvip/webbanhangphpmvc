<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <?php echo $this->_metaHTTP; ?>
    <?php echo $this->_metaName; ?>
    <title> <?php echo $this->_title;?> </title>
    <?php echo $this->_fileCss; ?>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!--    HEADER-->
    <?php include_once "html/header.php"?>
    <!--    HEADER-->

    <!--    SIDEBAR-->
    <?php include_once "html/sidebar.php"; ?>
    <!--    SIDEBAR-->

    <!-- Main content -->
    <?php require_once $this->_contentView;?>
    <!-- /.content -->

    <!--   FOOTER-->
    <?php include_once  "html/footer.php"?>
    <!--   FOOTER-->

</div>
<!-- ./wrapper -->
<?php echo $this->_fileJs;?>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
