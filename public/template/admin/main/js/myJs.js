function changeGroupStatus(url) {
    $.get(url, function (data){
        var id          = data[0];
        var status      = data[1];
        var link        = data[2];
        var element     = "a#groupStatus-" + id;
        var addClass    = "publish";
        var removeClass = "unpublish";
        if(data[1] == 0){
            addClass = "unpublish";
            removeClass = "publish";
        }

        // Change attr javascript
        $(element).attr("href", 'javascript:changeGroupStatus("'+data[2]+'")');
        // Change class
        $(element + ' span').addClass(addClass).removeClass(removeClass);

    }, 'json');
}

function changeGroupAcp(url) {
    $.get(url, function (data){
        var id          = data[0];
        var status      = data[1];
        var link        = data[2];
        var element     = "a#groupAcp-" + id;
        var addClass    = "publish";
        var removeClass = "unpublish";
        if(data[1] == 0){
            addClass = "unpublish";
            removeClass = "publish";
        }

        // Change attr javascript
        $(element).attr("href", 'javascript:changeGroupAcp("'+data[2]+'")');
        // Change class
        $(element + ' span').addClass(addClass).removeClass(removeClass);

    }, "json");
}

function submitForm(url) {
    $('#mainForm').attr('action', url);
    $('#mainForm').submit();
}
$(document).ready(function () {
    $('input[name=check-all-toggle]').change(function () {
        var checkStatus     = this.checked;
        $('#box-form').find(':checkbox').each(function () {
            this.checked = checkStatus;
        });
    });
});

